Backend
======

sapphire-backend communicates only over local filesystem-resident sockets, to
simplify jailing. To reach the outside world -- in particular, to benefit
from websockets -- it must be paired with sapphire-proxy, which implements
proxying, authentication, and SSL.

To set this up, use:

	$ ./sapphire-backend & ./sapphire-proxy

And if you need to restart, it's probably safest to quit both.

This process might be further streamlined in the future.

Proxy
=====

Historically, on a local single-user Sapphire instance, the architecture was
simple:

	Browser <--> Backend

However, this does not work for multi-user, public-facing instances. In these
cases, to minimise attack surface, this proxy is publicly facing while the
backends only listen to localhost and are sandboxed:

	Browser <--> Proxy (<--> Backends)

This package proxies WebSockets and implements authentication. It therefore
depends on glib/json-glib/libsoup, but it **MUST NOT** depend on libpurple
due to security isolation concerns.
