all: sapphire-backend sapphire-proxy
	echo "Bing"

# Deps: libpurple-dev, libglib2.0-dev, libjson-glib-dev
sapphire-backend: core.c websocket.c event-loop.c
	gcc `pkg-config --cflags glib-2.0` `pkg-config --cflags gio-unix-2.0` `pkg-config --cflags json-glib-1.0` core.c websocket.c event-loop.c -I/usr/include/libpurple `pkg-config --libs gobject-2.0` `pkg-config --libs json-glib-1.0` -lpurple -I/usr/include/libsoup-2.4 -lsoup-2.4 -Wall -Wextra -Werror -Wno-unused-parameter -o sapphire-backend -g 

# Deps: libglib2.0-dev, libsoup2.4-dev, libjson-glib-dev
sapphire-proxy: proxy.c
	gcc `pkg-config --cflags glib-2.0` `pkg-config --cflags gio-unix-2.0` `pkg-config --cflags json-glib-1.0` proxy.c `pkg-config --libs gobject-2.0` `pkg-config --libs json-glib-1.0` -lpurple -I/usr/include/libsoup-2.4 -lsoup-2.4 -Wall -Wextra -Werror -Wno-unused-parameter -o sapphire-proxy -g

clean:
	rm sapphire-backend sapphire-proxy
